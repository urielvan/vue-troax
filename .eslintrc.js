module.exports = {
  root: true,
  env: {
    browser: true,
    es6: true,
  },
  extends: [
    'plugin:vue/recommended',
    'eslint:recommended',
    '@vue/typescript/recommended'
  ],
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: '@typescript-eslint/parser',
  },
  plugins: [
    '@typescript-eslint',
  ],
  rules: {
    'array-bracket-newline': [
      'error',
      'consistent'
    ],
    'array-bracket-spacing': [
      'error',
      'always'
    ],
    'brace-style': 'error',
    'comma-dangle': ['error', {
      arrays: 'always-multiline',
      objects: 'always-multiline',
      imports: 'always-multiline',
      exports: 'always-multiline',
      functions: 'always-multiline',
    }],
    'comma-spacing': [
      'error',
      {
        after: true,
        before: false
      }
    ],
    'comma-style': [
      'error',
      'last'
    ],
    'computed-property-spacing': [
      'error',
      'always'
    ],
    'curly': 'error',
    'func-call-spacing': 'off',
    'indent': 'off',
    'keyword-spacing': [
      'error',
      {
        after: true,
        before: true
      }
    ],
    'newline-per-chained-call': [
      'error',
      {
        ignoreChainWithDepth: 3
      }
    ],
    'no-console': 'off',
    'no-dupe-class-members': 'off',
    'no-extra-semi': 'error',
    'no-fallthrough': 'off',
    'no-inner-declarations': 'off',
    'no-multi-spaces': 'error',
    'no-multiple-empty-lines': [
      'error',
      {
        max: 1
      }
    ],
    'no-trailing-spaces': 'error',
    'no-unused-vars': 'off',
    'object-curly-newline': [
      'error',
      {
        ImportDeclaration: {
          minProperties: 5
        },
        ObjectExpression: {
          multiline: true,
          consistent: true,
          minProperties: 2
        },
        ObjectPattern: {
          multiline: true
        }
      }
    ],
    'object-curly-spacing': [
      'error',
      'always'
    ],
    'padding-line-between-statements': [
      'error',
      {
        blankLine: 'always',
        next: '*',
        prev: [ 'const', 'let', 'var' ]
      },
      {
        blankLine: 'always',
        next: [ 'const', 'let', 'var' ],
        prev: '*'
      },
      {
        blankLine: 'never',
        next: [ 'const', 'let', 'var' ],
        prev: [ 'const', 'let', 'var' ]
      },
      {
        blankLine: 'always',
        next: 'return',
        prev: '*'
      },
      {
        blankLine: 'always',
        next: [
          'break',
          'multiline-expression',
          'if'
        ],
        prev: '*'
      },
      {
        blankLine: 'always',
        next: '*',
        prev: [
          'break',
          'multiline-expression',
          'if'
        ]
      },
      {
        blankLine: 'always',
        next: [
          'block-like',
          'function'
        ],
        prev: [
          'const',
          'var',
          'let'
        ]
      },
      {
        blankLine: 'never',
        next: 'expression',
        prev: 'expression'
      },
      {
        blankLine: 'always',
        next: 'multiline-expression',
        prev: 'expression'
      },
      {
        blankLine: 'always',
        next: 'expression',
        prev: 'multiline-expression'
      },
      {
        blankLine: 'always',
        next: [
          'const',
          'var',
          'let'
        ],
        prev: [
          'multiline-const',
          'multiline-var',
          'multiline-let'
        ]
      }
    ],
    'quotes': [
      'error',
      'single',
      {
        avoidEscape: true
      }
    ],
    'require-atomic-updates': 'off',
    'semi': 'off',
    'semi-spacing': [
      'error',
      {
        after: true,
        before: false
      }
    ],
    'space-before-blocks': 'error',
    'space-before-function-paren': [
      'error',
      'always'
    ],
    'space-in-parens': [
      'error',
      'always'
    ],
    'space-infix-ops': 'error',
    'space-unary-ops': 'error',
    '@typescript-eslint/func-call-spacing': [ 'error', 'always' ],
    '@typescript-eslint/indent': [
      'error',
      2,
      {
        ArrayExpression: 1,
        CallExpression: {
          arguments: 'first'
        },
        FunctionDeclaration: {
          body: 1,
          parameters: 'first'
        },
        ImportDeclaration: 1,
        MemberExpression: 1,
        ObjectExpression: 1,
        SwitchCase: 1,
        VariableDeclarator: 'first',
        outerIIFEBody: 1
      }
    ],
    '@typescript-eslint/no-namespace': [
      'error',
      {
        allowDeclarations: true
      }
    ],
    '@typescript-eslint/no-unused-vars': [
      'error',
      {
        ignoreRestSiblings: true,
        argsIgnorePattern: '^_'
      }
    ],
    '@typescript-eslint/no-use-before-define': [
      'error',
      {
        functions: false,
        classes: false
      }
    ],
    '@typescript-eslint/semi': [ 'error' ],
  },
};
