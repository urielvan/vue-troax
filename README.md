# Vue Demo

## 安装

由于 `npm install` 可能会修改 `package-lock.json` 文件，因此安装时推荐使用 `npm install --no-save`, 以避免修改

注: 由于国内网络环境限制，下载时速度极可能非常慢，因此推荐使用淘宝的镜像源

通过执行 `npm config set registry https://registry.npm.taobao.org` 进行修改

可以执行 `npm config get registry` 确认结果。

## 项目结构

1. 根目录中的文件为项目配置相关的文件

2. `public` 文件夹中为一些静态文件，后续使用的图片建议放入 `src/assets` 中，
具体的区别参考[文档](https://cli.vuejs.org/zh/guide/html-and-static-assets.html)

3. `src` 文件夹为源代码文件夹

   `components` 文件夹放页面公用的组件

   `stores` 文件夹放 vuex 相关，目前只是放了一个 `.gitkeep` 文件作占位用，添加文件后即可删除

   `styles` 文件夹放全局性的样式，后续新增的 sass 变量、函数、mixin 等都应放在这里，
   但是各页面或组件自用的样式不应放在这里

   `utils` 文件夹放一些公用的帮助函数等，目前同样只是放了个占位文件

   `views` 文件夹放各个页面

   `App.vue` 项目的根组件

   `main.ts` 项目的入口文件

   `router.ts` 项目的路由配置文件

   `shims-vue.d.ts` 对于 .vue 文件，typescript 无法正确导入，因此需要声明类型以正常使用

## 注意事项

1. vscode 需要安装一些插件以提升效率，具体参照之前的文档

2. 目前版本(0.24.0)的 Vetur 插件，在 .vue 文件中的 typescript 代码中从其他的 .vue 文件中导入组件会被提示错误，这个是插件本身的 bug，[GitHub](https://github.com/vuejs/vetur/issues/762)上这个问题的若干解决方案并不奏效，因此暂时只能忽略这个问题

3. **不要使用 class 组件**，这个特性由于所需的装饰器语法迟迟未能定稿，已经确定在后续的第3版 Vue 会被取消

4. typescript 有时可能会在组件中提示类型不兼容之类的错误，可以参考[文档](https://cn.vuejs.org/v2/guide/typescript.html#%E6%A0%87%E6%B3%A8%E8%BF%94%E5%9B%9E%E5%80%BC)，添加返回值尝试解决
