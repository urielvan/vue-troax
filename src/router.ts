import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Home from '@/views/Home.vue';
import News from '@/views/News.vue';

Vue.use ( VueRouter );

const routes: RouteConfig[] = [
  {
    path: '/',
    component: Home,
  },
  {
    path: '/news',
    component: News,
  },
];

export default new VueRouter ( {
  mode: 'history',
  routes,
} );
